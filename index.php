<?php
  /*
    Test Keys:

    Company ID: 3486
    Company Name: hawker.today
    Access Token: xC7sfGgtrPCcDnxRjcuh
    Secret Key: w-paKrUXx8VqiUV5mJLs

    Production Keys:

    Company ID: 3487
    Company Name: yihawker
    Access Token: HCapSArMmb9ji93gGMEf
    Secret Key: SxWtp1LJtLY7YUsxzM7B
  */
  header('Content-Type: application/json');
  header('Access-Control-Allow-Origin: *');

  $url = 'http://developer-api.bringg.com/partner_api/users';

  // $company_id = "3486";
  // $access_token = "xC7sfGgtrPCcDnxRjcuh";
  // $secret_key = "w-paKrUXx8VqiUV5mJLs";

  $company_id = "3487";
  $access_token = "HCapSArMmb9ji93gGMEf";
  $secret_key = "SxWtp1LJtLY7YUsxzM7B";

  $data_string = array(
    'access_token' => $access_token,
    'timestamp' => date('Y-m-d H:i:s'),
    'company_id' => $company_id
  );

  $signatue = hash_hmac("sha1", http_build_query($data_string), $secret_key);
  $data_string["signature"] = $signatue;
  $content = json_encode($data_string);

  $curl=curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
  curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
  curl_setopt($curl, CURLOPT_HTTPHEADER,
  array('Content-Type:application/json',
  'Content-Length: ' . strlen($content))
  );

  $json_response_customer = curl_exec($curl);

  $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

  if ( $status != 200 ) {
    die("Error: call to URL $url failed with status $status, response $json_response_customer, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
  }

  curl_close($curl);

  $response = json_decode($json_response_customer, true);

  print_r(json_encode($response));
?>